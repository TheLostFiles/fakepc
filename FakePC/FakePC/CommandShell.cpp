#include "CommandShell.h"
#include "ComputerMemory.h"
#include <fstream>
#include <iostream>

std::string CommandShell::IntakeCommandFromUser() 
{
	std::cout << ">";
	std::string userCommand; // makes a string
	getline(std::cin, userCommand); // gets line and makes the users input userCommand
	ProcessCommand(userCommand); // gets command Input
	return userCommand; // returns userCommand
}

void CommandShell::ProcessCommand(std::string commandToProcess)
{
	if(commandToProcess == "help" || commandToProcess == "Help")
	{
		Help(); // displays help
	}
	else if(commandToProcess == "displaymem" || commandToProcess == "DM" || commandToProcess == "DisplayMem")
	{
		DisplayMemory(); // displays what the memory has
	}
	else if(commandToProcess == "load" || commandToProcess == "Load")
	{
		Load(); // loads memory
	}
	else if(commandToProcess == "run" || commandToProcess == "Run")
	{
		Run(); // runs
	}
	else if(commandToProcess == "Exit")
	{
		//just pushes out of the loop
	}
	else
	{
		system("CLS"); // clears
		std::cout << "UNKNOWN COMMAND\n";
	}
}

void CommandShell::Help()
{
	system("CLS");
	std::cout << "THIS IS THE HELP MENU\n\n";
	std::cout << "Help - Displays this Help Menu\n";
	std::cout << "Load - Loads a program into memory\n";
	std::cout << "Run - Runs the program in memory\n";
	std::cout << "DisplayMem/DM - Displays all memory locations and values aka Dump memory\n";
	std::cout << "Exit - Exits this Shell\n";
}

void CommandShell::DisplayMemory()
{
	system("CLS"); // clears
	std::cout << "This is in memory\n";
	for (int i = 0; i < 100; i++)
	{
		std::cout << "\n Address = " << i << " " << "Value = " << GetRAM_Value(i); // Displays the memory
	}
	std::cout << "\n"; // next line
}

void CommandShell::Load()
{
	system("CLS"); // clears
	std::cout << "Loading file contents into memory\n";
	try // attempts to run the following code
	{
		std::string myText; // makes new string

		std::ifstream MyReadFile("Memory.txt"); // reads file

		int address = 0; // makes address 0;
		while (std::getline(MyReadFile, myText)) // while it is checking thing
		{
			SetRAM_Value(address++, myText); // sets things
		}

	}
	catch (...) // if an error occurs the code here runs
	{
		std::cout << "You really broke something to get here... \n"; // lets the user know that they should not be here
	}
}

void CommandShell::Run()
{
	system("CLS"); // clears
	std::cout << "Running content from memory\n";
	// TODO: put in killians code that we don't have
}