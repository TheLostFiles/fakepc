#pragma once
#include <string>
#include "ComputerMemory.h"

class CommandShell : ComputerMemory
{
public:
	
	std::string IntakeCommandFromUser(); // makes Intake Command From User
	
private:
	
	void ProcessCommand(std::string commandToProcess); // makes ProcessCommand
	void Help(); // makes help
	void DisplayMemory(); // makes DislpayMemory
	void Load(); // Makes Load
	void Run(); // makes run
};
