#include "ComputerMemory.h"

void ComputerMemory::SetRAM_Value(int address, std::string value)
{
	RAM[address] = value; // updates RAM to an address
}

std::string ComputerMemory::GetRAM_Value(int address)
{
	return RAM[address]; // returns address
}
