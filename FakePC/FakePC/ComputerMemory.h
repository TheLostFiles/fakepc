#pragma once
#include <string>

class ComputerMemory
{
public:
	void SetRAM_Value(int address, std::string value); // makes void setRAM_value
	std::string GetRAM_Value(int address); // makes string GetRAM_Value
	
private:
	std::string RAM[100];//0-99
	
};
