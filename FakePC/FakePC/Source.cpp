#include <iostream>

#include "CommandShell.h"
#include "ComputerMemory.h"

void main()
{
	std::cout << "LINK START!\n";
	CommandShell command_shell; // makes a command shell 
	std::cout << "Start By Typing a Command\n";

	do
	{
		command_shell.IntakeCommandFromUser(); // makes userCommand something that can be taken in in command shell source file
	}
	while (command_shell.IntakeCommandFromUser() != "Exit"); // checks if the user typed exit

	std::cout << "Thank you Goodbye now";
}